package br.com.apiproducts.v1.services;

import br.com.apiproducts.v1.domain.model.BaseModel;
import br.com.apiproducts.v1.repository.BaseRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

public abstract class BaseService<T extends BaseModel> implements IBaseService<T> {

    protected abstract BaseRepository<T, String> getRepository();

    @Override
    @Transactional(readOnly = true)
    public Optional<T> findById(String id){
        return getRepository().findById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public List<T> findAll(){
        return getRepository().findAll();
    }

    @Override
    @Transactional
    public T save(T t){
        return getRepository().save(t);
    }

    @Override
    @Transactional
    public void remove(T t) {
        getRepository().delete(t);
    }

}
