package br.com.apiproducts.v1.services;

import br.com.apiproducts.v1.domain.model.Product;
import br.com.apiproducts.v1.exception.ApiException;
import br.com.apiproducts.v1.repository.BaseRepository;
import br.com.apiproducts.v1.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductService extends BaseService<Product> implements IProductService {

    @Autowired
    private ProductRepository repository;

    @Override
    protected BaseRepository<Product, String> getRepository() {
        return repository;
    }

    @Override
    public Optional<Product> findById(String id) {
        return getRepository().findById(id);
    }

    @Override
    public Optional<Product> findById(long id) {
        return Optional.empty();
    }

    @Override
    public List<Product> findAll() {
        return getRepository().findAll();
    }

    @Override
    public Product save(Product product) {
        return getRepository().save(product);
    }

    @Override
    public void remove(Product product) {
        getRepository().delete(product);
    }

    @Override
    public List<Product> findFiltred(BigDecimal minPrice, BigDecimal maxPrice) {

        if (minPrice == null || maxPrice == null) {
            return getRepository().findAll();
        } else {
            return getProductsFiltred(minPrice, maxPrice, repository.getProductsPerInterval(minPrice, maxPrice));
        }
    }

    //ajuste para gerar o filtro 'between' ...
    private List<Product> getProductsFiltred(BigDecimal minPrice, BigDecimal maxPrice, List<Product> productsAll) {
        List<Product> productList = new ArrayList<>();

        for (Product product :  productsAll) {
            if(
                (product.getPrice().compareTo(minPrice) == 1 || product.getPrice().compareTo(minPrice) == 0)
                &&
                (product.getPrice().compareTo(maxPrice) == -1 || product.getPrice().compareTo(maxPrice) == 0)
            ){
                productList.add(product);
            }
        }
        return productList;
    }
}
