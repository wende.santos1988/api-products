package br.com.apiproducts.v1.services;

import br.com.apiproducts.v1.domain.model.Product;

public interface IProductService extends IBaseService<Product> {
}
