package br.com.apiproducts.v1.services;

import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface IBaseService<T> {

    Optional<T> findById(String id);

    @Transactional(readOnly = true)
    Optional<T> findById(long id);

    List<T> findAll();

    T save(T t);

    void remove(T t);

    List<T> findFiltred(BigDecimal min_price, BigDecimal max_price);
}
