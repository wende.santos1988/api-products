package br.com.apiproducts.v1.domain.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductSummaryModelDTO {

    private String id;
    private String name;
    private String description;

}
