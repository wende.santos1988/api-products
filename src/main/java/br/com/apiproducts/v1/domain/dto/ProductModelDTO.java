package br.com.apiproducts.v1.domain.dto;

import br.com.apiproducts.v1.domain.enums.StatusEnum;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class ProductModelDTO {

    private String id;
    private String name;
    private String description;
    private BigDecimal price;
    private StatusEnum status;

}
