package br.com.apiproducts.v1.domain.enums;

public enum StatusEnum {
    AVAILABLE("AVAILABLE"),
    OUT_OF_STOCK("OUT OF STOCK");

    public final String value;

    StatusEnum(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
