package br.com.apiproducts.v1.domain.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.envers.AuditTable;
import org.hibernate.envers.Audited;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Audited
@Entity
//@Table(name = "PRODUCT")
@Document(collection = "PRODUCT")
@AuditTable(value = "aud_product_log")
@Getter
@Setter
@ToString
public class Product extends BaseModel implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private String id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name = "PRICE")
    private BigDecimal price;

    @Column(name = "QUANTITY")
    private Integer quantity;

    public Product(String name, String description, BigDecimal price){
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public Product() {}

}
