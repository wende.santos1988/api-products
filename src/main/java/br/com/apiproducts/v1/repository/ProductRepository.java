package br.com.apiproducts.v1.repository;

import br.com.apiproducts.v1.domain.model.Product;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface ProductRepository extends BaseRepository<Product, String> {

    Optional<Product> findById(String id);

//    @Query(value = "SELECT * FROM PRODUCT WHERE price BETWEEN :min_price AND :max_price ORDER BY price", nativeQuery = true)
//    Optional<List<Product>> findPriceFiltred(@Param("min_price") BigDecimal min_price, @Param("max_price") BigDecimal max_price);

    @Query("{'price':{$gte:?0 ,$lte:?1}}")
    List<Product> getProductsPerInterval(BigDecimal minPrice, BigDecimal maxPrice);

}
