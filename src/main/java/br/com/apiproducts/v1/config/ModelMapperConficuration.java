package br.com.apiproducts.v1.config;

import br.com.apiproducts.v1.domain.dto.ProductModelDTO;
import br.com.apiproducts.v1.domain.dto.ProductSummaryModelDTO;
import br.com.apiproducts.v1.domain.enums.StatusEnum;
import br.com.apiproducts.v1.domain.model.Product;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ModelMapperConficuration {

    @Bean
    public ModelMapper modelMapper() {

        ModelMapper modelMapper = new ModelMapper();

        Converter<Integer, StatusEnum> situacaoEstoqueEnumConverter =
                ctx -> ctx.getSource() > 0 ? StatusEnum.AVAILABLE : StatusEnum.OUT_OF_STOCK;

        modelMapper.createTypeMap(Product.class, ProductModelDTO.class)
            .addMappings(mapper -> mapper.using(situacaoEstoqueEnumConverter)
                .map(Product::getQuantity, ProductModelDTO::setStatus));

        return modelMapper;
    }

}
