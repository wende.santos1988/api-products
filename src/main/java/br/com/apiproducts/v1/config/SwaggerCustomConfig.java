package br.com.apiproducts.v1.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerCustomConfig {

    @Bean
    public OpenAPI customOpenAPI(@Value("${titulo-projeto}") String titulo,
                                 @Value("${descricao-projeto}") String descricao,
                                 @Value("${versao-projeto}") String versao) {
        return new OpenAPI()
                .info(new Info()
                        .title(titulo)
                        .version(versao)
                        .description(descricao)
                        .license(new License().name("Apache 2.0").url("http://springdoc.org")));
    }

}
