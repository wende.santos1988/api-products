package br.com.apiproducts.v1.controller;

import br.com.apiproducts.v1.domain.dto.ProductModelDTO;
import br.com.apiproducts.v1.domain.dto.ProductSummaryModelDTO;
import br.com.apiproducts.v1.domain.model.BaseModel;
import br.com.apiproducts.v1.domain.model.Product;
import br.com.apiproducts.v1.exception.ApiException;
import br.com.apiproducts.v1.services.IBaseService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.UnexpectedRollbackException;

import javax.persistence.RollbackException;
import javax.validation.ConstraintViolationException;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Optional;

@Slf4j
public abstract class AbstractRestController<T extends BaseModel, TS, TM> {

    protected abstract IBaseService<T> getService();

    private String entityName;

    public ResponseEntity<List<T>> list(){
        return ResponseEntity.ok(getService().findAll());
    }

    public ResponseEntity<T> findOne(String id){

        log.debug("REST request to get {} : {}",getEntityName(), id);

        Optional<T> t = getService().findById(id);

        if (!t.isPresent()){
//            throw new ApiException("Product not found", 404);
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(t.get());
    }

    public ResponseEntity save(T t){
        log.debug("REST request to save "+getEntityName()+" : {}", t);

        getService().save(t);

        return ResponseEntity.ok(t);
    }

    public ResponseEntity edit(String id, T t){
        log.debug("REST request to update " + getEntityName() +" : {}", t);
        getService().save(t);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    public ResponseEntity delete(String id){
        log.debug("REST request to delete {} : {}", getEntityName(), id);
        Optional<T> t = getService().findById(id);

        if (!t.isPresent())
            throw new ApiException("Product not found", 404);

        try {
            getService().remove(t.get());
        }catch (RollbackException | UnexpectedRollbackException | DataIntegrityViolationException | ConstraintViolationException | org.hibernate.exception.ConstraintViolationException e) {
            throw new ApiException("The record has links in the system and cannot be deleted.", 400);
        }


        return ResponseEntity.noContent().build();
    }

    protected String getEntityName(){
        if (entityName == null)
            entityName = ((Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0]).getSimpleName();

        return entityName;
    }

}
