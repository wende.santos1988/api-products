package br.com.apiproducts.v1.controller;

import br.com.apiproducts.v1.domain.dto.ProductModelDTO;
import br.com.apiproducts.v1.domain.dto.ProductSummaryModelDTO;
import br.com.apiproducts.v1.domain.model.Product;
import br.com.apiproducts.v1.services.IBaseService;
import br.com.apiproducts.v1.services.ProductService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("v1/product")
public class ProductController extends AbstractRestController<Product, ProductSummaryModelDTO, ProductModelDTO>{

    @Autowired
    ProductService service;

    @Override
    protected IBaseService<Product> getService() {
        return service;
    }

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<ProductSummaryModelDTO>> listAll() {

        return ResponseEntity.ok(getService()
                .findAll()
                .stream()
                .map(this::toProductSummaryModelDTO)
                .collect(Collectors.toList()));
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductModelDTO> findOneProduct(@PathVariable String id) throws Exception {

        Optional<Product> product = Optional.ofNullable(getService().findById(id).orElseThrow(Exception::new));

        return ResponseEntity.ok(this.toProductModel(product.orElse(null)));
    }

    @Override
    @PostMapping
    public ResponseEntity save(@RequestBody Product product) {
        return super.save(product);
    }

    @Override
    @PutMapping("/{id}")
    public ResponseEntity edit(@PathVariable String id, @RequestBody Product product) {
        return super.edit(id, product);
    }

    @Override
    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable String id) {
        return super.delete(id);
    }

    @GetMapping("/search")
    public ResponseEntity<List<Product>> findFiltred(@RequestParam String minPrice, @RequestParam String maxPrice) {

        BigDecimal min = BigDecimal.ZERO;
        BigDecimal max = BigDecimal.ZERO;

        try {
            min = min.add(new BigDecimal(minPrice));
            max = max.add(new BigDecimal(maxPrice));
        }catch (Exception e) {
            System.out.println("error :: " + e.getCause() + " - " + e.getMessage());
            return ResponseEntity.badRequest().build();
        }

        List<Product> products = getService().findFiltred(min, max);

        Collections.sort(products, (p1, p2) -> p1.getPrice().compareTo(p2.getPrice()));

        return ResponseEntity.ok(products);

    }

    protected ProductModelDTO toProductModel(Product product) {
        return modelMapper.map(product, ProductModelDTO.class);
    }

    protected ProductSummaryModelDTO toProductSummaryModelDTO(Product product) {
        return modelMapper.map(product, ProductSummaryModelDTO.class);
    }

}
